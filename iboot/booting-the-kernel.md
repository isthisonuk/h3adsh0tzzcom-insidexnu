---
title: Booting the Kernel
---

So far in this series we have discussed what iBoot is and the role it plays, the multi-tasking implementation and what tasks are, and in the previous post looked more in-depth in terms of the main task. We also covered the boot modes, such as Recovery Mode and Normal Boot, along with the Image4 format used for iOS, and now macOS, firmware files. I plan to dedicate a blog post to the Image4 format in the future.

This is the last entry into what is essentially Chapter 1 covering iBoot. Here we will discuss and look at how iBoot finds a kernel image, the loading and verification of that kernel image, and how it jumps from iBoot to Kernel mode. While we will mention the Kernelcache format here a lot, that will instead be saved for the first entry into Chapter 2.

## Autoboot

iBoot has something called "Autoboot" for determining what should be booted and where the boot files are. Autoboot is invoked with `do_iboot_autoboot()` just before Recovery Mode setup begins. So should autoboot fail and that call returns, iBoot will automatically default to Recovery Mode. 

On release devices there are three autoboot options: fsboot, diags and upgrade. Debug devices have a script option, but that's not covered here. Our focus will be on fsboot, but the other two are fairly obvious as to their function. The `do_iboot_autoboot()` function has the following flow:

 * Define possible boot options and their "handlers"
 * Initialise mass storage devices
 * Check that autoboot has not already failed 5 times. If so, abort.
 * Check for manual Recovery Mode request, if requested return.
 * Perform Common Autoboot.

Let's look at these one-by-one, starting with defining the boot options. We have an `autoboot_command` C structure that defines a command name and a function pointer to handle that command, which can then be used to create a set of different boot commands and their handlers.

```c
struct autoboot_command {
    const char      *command;
    int             (*func)(void);
};
...
    static struct autoboot_command example_commands[] = {
        {"command1", boot_command_1},
        {"command2", boot_command_2},
        { NULL, NULL}
    };
...
int boot_command_1 (void) { ... }
int boot_command_2 (void) { ... }
```

These will be useful later. The second step is initialising mass storage, which isn't a particularly interesting step. Depending on the device this could be either an NVME or NAND storage device, either way this step is required as the XNU kernel is placed at /System/Library/Caches/com.apple.kernelcaches/kernelcache on the devices storage.

Next is to check that this processed hasn't already failed. There is a limit built-in of 5 attempts so that a device cannot endlessly bootloop without being able to get into either DFU or Recovery mode. While bootloops can still occur, without this limit the device would just loop until it died, making it difficult to attempt to recover the device. Should this occur, and the boot has failed 5 or more times, iBoot just drops back to the main task where the next operation is recovery mode setup. Step 4 checks if the correct button sequence for recovery mode has been entered by the user, and like step 3 if this occurs we just drop back to the main task where recovery mode can boot.

Finally, should the storage device successfully initialise, the loop-check passes, and no recovery mode request is made, autoboot will continue by invoking `do_common_autoboot()` and passing the list of boot commands.

### Common Autoboot

Environment variables play a big part here. Looking back to the Introductory post you may remember the `_main_generic()` function where the main task was created. Now, just before the main task was created there is, although this was not shown, a call to `sys_int()` which sets up default environment variables. The way the environment variable system works is as follows:

 * Variables can be set using `env_set()`, or by specifying a type such as `env_set_uint()`.
 * Variables can be read using `env_get()`, or by specifying a type such as `env_get_bool()`.
 * Variables can be unset using `env_unset()`.
 * Environment variables are written to NVRAM, meaning they should persist after a reboot.

For the time being we only need to concern ourselves with three environment variables: `boot-command`, `auto-boot` and `bootdelay`. These have default values set accordingly:

 * auto-boot is initially set to `true` (`false` for debug iBoot).
 * boot-command is initially set to `fsboot`.
 * bootdelay is initially set to `3`.

There is also the `boot-path` and `boot-ramdisk` variables, used a little later:

 * boot-path is initially set to `/System/Library/Caches/com.apple.kernelcaches/kernelcache`.
 * boot-ramdisk is not given an initial value, and appears to be debug-only.

The value of `auto-boot` is checked first, if this value is set to true - which it should be on RELEASE devices - the device will wait until the `bootdelay` has passed and then read the value of `boot-command`. If `boot-command` is not set the device will end up in recovery mode. iBoot then records the current system time, and loops through the list of boot commands given as an argument, comparing them to the environment variable `boot-command`. Should one of them match, the command handler is invoked, otherwise recovery mode would be booted. Let's try to visualise this:

```c
...
    autoboot = env_get('auto-boot');

    if (autoboot) {

        /* delay boot */
        uint32_t delayboot = env_get_uint('delayboot');
        uint32_t t = system_time();

        while (1) {
            if ((system_time()) - t > (bootdelay * 1000000))
                break;

            task_sleep (10 * 1000)
        }

        /* verify boot-command */
        const char *boot_command = env_get('boot-command');

        if (!boot_command) 
            return;      /* falls back to recovery mode */

        /* record current time */
        k_time = system_time();

        for (int i = 0; NULL != cmds[i].command; i++) {
            if (!strcmp(boot_command, cmds[i].command))
                cmd[i].func();
        }

        /* autoboot failed */
        return;
    }

```

We are now in a situation where whatever was placed in `boot-command`, remember `fsboot` is the default, should now have been selected. The handler for fsboot was `mount_and_boot_system`, so this is where we are now!

### Mounting the filesystem

We're getting close to jumping to the kernel. The process here is to mount the filesystem, find the kernel, decompress/extract and verify the kernel cache Image4, load the kernel into memory and then jump to it - after that we move onto covering XNU.

The first step here is to obtain the location of the Kernelcache on the filesystem. This is saved in the environment variable `boot-path` which we just covered, and this is where iBoot will search for the Kernel. If for whatever reason this variable is not set, the default action is, like before, to drop back into Recovery Mode. In this case upon a fail iBoot will also attempt to unmount the `/boot` filesystem.

Briefly, iBoot will perform some platform-specific operations which follow this flow:

 * Load the `boot-device` variable, this is platform-specific so NVME, etc.
 * Load the `boot-partition` variable, this is common across all devices.
 * Define `bootsubdevice` as `bootdevice + boot-partition + 'a'`. Using t8010 as an example:
    * `boot-device = "nvme_nand0"`
    * `boot-partition = 0`
    * `result = "nvme_nand00a"`
 * Use this `bootsubdevice` to search the "blockdev".

A matching block device will be interated over to find any filesystems. Each time a filesystem is found, we would try to mount the `/boot` partition. Once this is successful we can continue.

### Loading the Kernelcache



- load_kernelcache_file()
    - secure_addr = DEFAULT_KERNEL_ADDRESS;
    - secure_size = DEFAULT_KENREL_SIZE;
        - these values are set depending on device platform
    - path = "/boot" + "/System/Library/Caches/com.apple.kernelcaches/kernelcache"
    - entry = NULL
    - boot_args = NULL

    - image_load_file(path, &secure_addr, &secure_size, &type, 1, NULL, 0)
        - const char *path
        - addr_t *address
        - size_t *length
        - const u_int32_t *types
        - u_int32_t count
        - u_int32_t *actual
        - u_int32_t options

        - ```struct image_info {
            u_int32_t	imageLength;
            u_int32_t	imageAllocation;
            u_int32_t	imageType;
            u_int32_t	imagePrivateMagic;
        #define IMAGE_MEMORY_INFO_MAGIC		'Memz'
        #define IMAGE2_IMAGE_INFO_MAGIC		'img2'
        #define IMAGE3_IMAGE_INFO_MAGIC		'img3'
        #define IMAGE4_IMAGE_INFO_MAGIC		'img4'
            u_int32_t	imageOptions;
            void		*imagePrivate;
        }```

        - image = image_create_from_memory(DEFAULT_LOAD_ADDRESS, *length, options)
        - result = image_load (image, types, count, actual, (void **) address, length)

        - image_free()



- mount_and_boot_system
    - load the boot-path from the env var
    - call mount_bootfs()
        - grab boot-device
        - check it's valid
        - bootsubdevice = env_get_uint('boot-partition') + 'a') 
        - return mount_bootfs_from_device(bootsubdevice)
            - use lookup_bdev to check if the subdevice exists
            - iterate over the filesystems to find one that will mount the partition.
            - don't go into too much detail with this, probably not necassary.
            - return 0 for success, -1 for failure
    - optional bootramdisk
        -
    - load_kernelcache_file()

    - boot_darwin()
    
    - failure, panic() then fs_unmount('/boot')





- env's set:
    - auto-boot: whether iboot should continue or drop into debugger

    - boot-command: 
        - in main_generic, before the main task is started, sys_init() is called
            - sys_init(), among other things, invokes sys_setup_default_environment()
            - sys_setup_default_environment() initialises environemnt variables, such as auto-boot and boot-command
                - default, non-debug values are:
                    "boot-command" = "fsboot"
                    "auto-boot" = "true"

                - Other things like build style and version are set
                - boot-partition is set to 0
                - boot-path is set to "/System/Library/Caches/com.apple.kernelcaches/kernelcache"

                - load-addr is set to DEFAULT_LOAD_ADDRESS.
                - boot-device is set based on the device.


## ....