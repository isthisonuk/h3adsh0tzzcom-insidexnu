---
layout: page
title: iBoot Profiles
permalink: /inside-xnu/iboot/profiles
---

[REWRITE THE INTRODUCTION HERE]

Before I move on to looking into iBoot's `main_task`, I feel I should cover this beforehand as it's not exclusive to the `main_task` and will probably raise some questions. iBoot has what Apple have called a "Profiling System". You'll probably already know about Kernel Panic's, well iBoot has a panic system too, and it's related to the profiles.

```c
__int64 __fastcall
main_task (void *arg)
{
    ...
    PROFILE_ENTER('ScI');
    security_init(true);
    PROFILE_EXIT('ScI');
    ...
}
```

Take the above code, you'll notice the calls to `PROFILE_ENTER` and `PROFILE_EXIT` with a short string being passed to them to represent the "profile". These two calls make something called a `profile_event` entry into a global `profile_buffer`. They generate what I guess one could call an ID from the string passed, called a "tag". This tag is slightly different depending on whether an enter or exit call is made. They both call the same function for creating these `profile_event`'s. Let's look at them.

```c
#if WITH_PROFILE

struct profile_event {
    uint32_t    time;
    addr_t      pc;
    uint32_t    data1;
    uint32_t    data2;
};

struct profile_buffer {
    uint32_t    magic;
    uint32_t    count;
    uint32_t    pc_bytes;
    uint32_t    timer_ticks_per_sec;
    uint32_t profile_event  events[];
};

#define PROFILE_2(_data1, _data2) \
	do { \
		addr_t current_pc = 0; \
		__asm__ volatile ("L_%=: adr %0, L_%=" : "=r"(current_pc)); \
		profile(current_pc, (_data1), (_data2)); \
	} while(0)

#else   //!WITH_PROFILE
    ...
#endif


#define PROFILE_ENTER(tag)          PROFILE_2(((tag) & 0xFFFFFF) | ('<' << 24), 0)
#define PROFILE_EXIT(tag)           PROFILE_2(((tag) & 0xFFFFFF) | ('>' << 24), 0)

...
static struct profile_buffer *profile_buffer = (struct profile_buffer *) PANIC_BASE;

```

There are two structures defined, `profile_buffer` and `profile_event`. The `profile_buffer` structure is particularly interesting as it is defined as a pointer to `PANIC_BASE` which is read when iBoot panic's. This is the reasoning behind my analysis that the profile system is used to log events for them to subsequently be read back if and when iBoot panic's. The `profile_buffer` is made up of four properties, along with an array of `profile_event` structs, however, only the `events[]` property is filled in iBoot. iBSS and LLB do populate the `magic`, `count`, `pc_bytes` and `timer_ticks_per_sec` properties, though.

So, before iBoot does a setup routine, it calls `PROFILE_ENTER` with a tag, and calls `PROFILE_EXIT` with the same tag once the setup routine returns. These two macro's both call `PROFILE_2` by passing `_data1` as the tag, with some maths applied to it, and `0` as `_data2`. The `PROFILE_2` macro reads the current value of the program counter into `current_pc`, and calls `profile()`.

Now, `profile()` takes the two data arguments, and the program counter value. It will first check that the `profile_buffer->count` value is less than `MAX_PROFILE_ENTRIES`, which is defined as `((PROFILE_BUF_SIZE - sizeof(struct profile_buffer)) / sizeof(struct profile_event))` with `PROFILE_BUF_SIZE` being `0x400`. This tells us that the max amount of profiles that can be added to the profile buffer is `63`. The aim of the `profile()` function is to create and populate a new `profile_entry` and then add it to `profile_buffer->events`. The new `profile_event` structure is initialised as a reference to the event at `count++`, like so: `next_entry = &profile_buffer->events[profile_buffer->count++];`. The other values, being `pc`, `data1` and `data2` are set accordingly and `time` is set by calling `timer_get_ticks()`. 

Finally, there is a `profile_handoff()` function. This is only called when preparing to jump to a loaded image and copies the `profile_buffer` to `PANIC_BASE` providing it is not already there. 
