---
title: Inside XNU
---

Inside XNU is my blog-series, and maybe one day a book, where I explore, analyse and discuss the boot process and functionality of the XNU Kernel used on Apple Darwin-based Operating Systems. We also briefly cover iBoot as it's functionality is important in understanding how XNU is loaded and operates.

My inspiration for this project comes from a few places. Firstly is my interest in operating systems, bootloaders and kernels, particularly my interest in Darwin. Second is a project by [@0xAX]() where he covers the Linux Kernel is quite some detail, and Third is that there is no real alternative to 0xAX's work for XNU, although Jonathan Levin's books do cover certain topics regarding security in detail.

The format of this series is, as mentioned, blog posts. They will typically vary in length, and I have no set or predefined schedule that I'm aiming to follow as this is something I look into when I have some free time and make notes on. 

We will look at XNU from an AArch64 point of view. That is the architecture that I feel is the easier one to understand and follow, it's the architecture I have the most knowledge and (limited) experience with, and with most of the devices XNU runs on being AArch64-based, it made the most sense.

Like I've already mentioned, there's no set timeframe for these blog updates. Ideally I'd like to do one per month, and more if I have the time. This is not a topic I'm an expert in, this is kind of a learning excercise and somewhere for me to publish my own notes, so if you do notice something that doesn't look quite right, please let me know!