---
title: AArch64 Kernel Initialisation
---

Kernel initialisation - this is the first topic we will cover. This will take us from the very first code that iBoot loads and runs of the Kernel, up to the generic, non-architecture specific code in the OSFMK portion of XNU. 

< cont >