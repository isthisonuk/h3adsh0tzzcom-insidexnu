---
title: Common CPU Initialisation
---


- what we've already discussed
- what is meant by "Common CPU Initialisation"

### `common_start`

#### mainting `arm_init` in the link register
```
common_start:

#if HAS_NEX_PG
	mov x19, lr
	bl		EXT(set_nex_pg)
	mov lr, x19
#endif
```

#### translation control register
```
// Set the translation control register.
	adrp	x0,     EXT(sysreg_restore)@page		// Load TCR value from the system register restore structure
	add		x0, x0, EXT(sysreg_restore)@pageoff
	ldr		x1, [x0, SR_RESTORE_TCR_EL1]
	MSR_TCR_EL1_X1

	 *	TTBR0 - V=P table @ top of kernel
	 *	TTBR1 - KVA table @ top of kernel + 1 page
	 */
#if defined(KERNEL_INTEGRITY_KTRR) || defined(KERNEL_INTEGRITY_CTRR)
	/* Note that for KTRR configurations, the V=P map will be modified by
	 * arm_vm_init.c.
	 */
#endif
	and		x0, x25, #(TTBR_BADDR_MASK)
	mov		x19, lr
	bl		EXT(set_mmu_ttb)
	mov		lr, x19
	add		x0, x25, PGBYTES
	and		x0, x0, #(TTBR_BADDR_MASK)
	MSR_TTBR1_EL1_X0
```

#### memory attribute indirection registers
```
// Set up MAIR attr0 for normal memory, attr1 for device memory
	mov		x0, xzr
	mov		x1, #(MAIR_WRITEBACK << MAIR_ATTR_SHIFT(CACHE_ATTRINDX_WRITEBACK))
	orr		x0, x0, x1
	mov		x1, #(MAIR_INNERWRITEBACK << MAIR_ATTR_SHIFT(CACHE_ATTRINDX_INNERWRITEBACK))
	orr		x0, x0, x1
	mov		x1, #(MAIR_DISABLE << MAIR_ATTR_SHIFT(CACHE_ATTRINDX_DISABLE))
	orr		x0, x0, x1
	mov		x1, #(MAIR_WRITETHRU << MAIR_ATTR_SHIFT(CACHE_ATTRINDX_WRITETHRU))
	orr		x0, x0, x1
	mov		x1, #(MAIR_WRITECOMB << MAIR_ATTR_SHIFT(CACHE_ATTRINDX_WRITECOMB))
	orr		x0, x0, x1
	mov		x1, #(MAIR_POSTED << MAIR_ATTR_SHIFT(CACHE_ATTRINDX_POSTED))
	orr		x0, x0, x1
	mov		x1, #(MAIR_POSTED_REORDERED << MAIR_ATTR_SHIFT(CACHE_ATTRINDX_POSTED_REORDERED))
	orr		x0, x0, x1
	mov		x1, #(MAIR_POSTED_COMBINED_REORDERED << MAIR_ATTR_SHIFT(CACHE_ATTRINDX_POSTED_COMBINED_REORDERED))
	orr		x0, x0, x1
	msr		MAIR_EL1, x0
	isb
	tlbi	vmalle1
	dsb		ish
```

#### timer interrupts
```
#if defined(BCM2837)
	// Setup timer interrupt routing; must be done before MMU is enabled
	mrs		x15, MPIDR_EL1						// Load MPIDR to get CPU number
	and		x15, x15, #0xFF						// CPU number is in MPIDR Affinity Level 0
	mov		x0, #0x4000
	lsl		x0, x0, #16
	add		x0, x0, #0x0040						// x0: 0x4000004X Core Timers interrupt control
	add		x0, x0, x15, lsl #2
	mov		w1, #0xF0 						// x1: 0xF0 	  Route to Core FIQs
	str		w1, [x0]
	isb		sy
#endif
```

#### work out the rest into sections
```

#ifndef __ARM_IC_NOALIAS_ICACHE__
	/* Invalidate the TLB and icache on systems that do not guarantee that the
	 * caches are invalidated on reset.
	 */
	tlbi	vmalle1
	ic		iallu
#endif

	/* If x21 is not 0, then this is either the start_cpu path or
	 * the resume_idle_cpu path.  cpu_ttep should already be
	 * populated, so just switch to the kernel_pmap now.
	 */

	cbz		x21, 1f
	adrp	x0, EXT(cpu_ttep)@page
	add		x0, x0, EXT(cpu_ttep)@pageoff
	ldr		x0, [x0]
	MSR_TTBR1_EL1_X0
1:

	// Set up the exception vectors
#if __ARM_KERNEL_PROTECT__
	/* If this is not the first reset of the boot CPU, the alternate mapping
	 * for the exception vectors will be set up, so use it.  Otherwise, we
	 * should use the mapping located in the kernelcache mapping.
	 */
	MOV64	x0, ARM_KERNEL_PROTECT_EXCEPTION_START

	cbnz		x21, 1f
#endif /* __ARM_KERNEL_PROTECT__ */
	adrp	x0, EXT(ExceptionVectorsBase)@page			// Load exception vectors base address
	add		x0, x0, EXT(ExceptionVectorsBase)@pageoff
	add		x0, x0, x22									// Convert exception vector address to KVA
	sub		x0, x0, x23
1:
	MSR_VBAR_EL1_X0

1:
#ifdef HAS_APPLE_PAC
#ifdef __APSTS_SUPPORTED__
	mrs		x0, ARM64_REG_APSTS_EL1
	and		x1, x0, #(APSTS_EL1_MKEYVld)
	cbz		x1, 1b 										// Poll APSTS_EL1.MKEYVld
	mrs		x0, ARM64_REG_APCTL_EL1
	orr		x0, x0, #(APCTL_EL1_AppleMode)
#ifdef HAS_APCTL_EL1_USERKEYEN
	orr		x0, x0, #(APCTL_EL1_UserKeyEn)
	and		x0, x0, #~(APCTL_EL1_KernKeyEn)
#else /* !HAS_APCTL_EL1_USERKEYEN */
	orr		x0, x0, #(APCTL_EL1_KernKeyEn)
#endif /* HAS_APCTL_EL1_USERKEYEN */
	and		x0, x0, #~(APCTL_EL1_EnAPKey0)
	msr		ARM64_REG_APCTL_EL1, x0


#else
	mrs		x0, ARM64_REG_APCTL_EL1
	and		x1, x0, #(APCTL_EL1_MKEYVld)
	cbz		x1, 1b 										// Poll APCTL_EL1.MKEYVld
	orr		x0, x0, #(APCTL_EL1_AppleMode)
	orr		x0, x0, #(APCTL_EL1_KernKeyEn)
	msr		ARM64_REG_APCTL_EL1, x0
#endif /* APSTS_SUPPORTED */

	/* ISB necessary to ensure APCTL_EL1_AppleMode logic enabled before proceeding */
	isb		sy
	/* Load static kernel key diversification values */
	ldr		x0, =KERNEL_ROP_ID
	/* set ROP key. must write at least once to pickup mkey per boot diversification */
	msr		APIBKeyLo_EL1, x0
	add		x0, x0, #1
	msr		APIBKeyHi_EL1, x0
	add		x0, x0, #1
	msr		APDBKeyLo_EL1, x0
	add		x0, x0, #1
	msr		APDBKeyHi_EL1, x0
	add		x0, x0, #1
	msr		ARM64_REG_KERNELKEYLO_EL1, x0
	add		x0, x0, #1
	msr		ARM64_REG_KERNELKEYHI_EL1, x0
	/* set JOP key. must write at least once to pickup mkey per boot diversification */
	add		x0, x0, #1
	msr		APIAKeyLo_EL1, x0
	add		x0, x0, #1
	msr		APIAKeyHi_EL1, x0
	add		x0, x0, #1
	msr		APDAKeyLo_EL1, x0
	add		x0, x0, #1
	msr		APDAKeyHi_EL1, x0
	/* set G key */
	add		x0, x0, #1
	msr		APGAKeyLo_EL1, x0
	add		x0, x0, #1
	msr		APGAKeyHi_EL1, x0

	// Enable caches, MMU, ROP and JOP
	MOV64	x0, SCTLR_EL1_DEFAULT
	orr		x0, x0, #(SCTLR_PACIB_ENABLED) /* IB is ROP */

#if __APCFG_SUPPORTED__
	// for APCFG systems, JOP keys are always on for EL1.
	// JOP keys for EL0 will be toggled on the first time we pmap_switch to a pmap that has JOP enabled
#else /* __APCFG_SUPPORTED__ */
	MOV64	x1, SCTLR_JOP_KEYS_ENABLED
	orr 	x0, x0, x1
#endif /* !__APCFG_SUPPORTED__ */
#else  /* HAS_APPLE_PAC */

	// Enable caches and MMU
	MOV64	x0, SCTLR_EL1_DEFAULT
#endif /* HAS_APPLE_PAC */
	MSR_SCTLR_EL1_X0
	isb		sy

	MOV64	x1, SCTLR_EL1_DEFAULT
#if HAS_APPLE_PAC
	orr		x1, x1, #(SCTLR_PACIB_ENABLED)
#if !__APCFG_SUPPORTED__
	MOV64	x2, SCTLR_JOP_KEYS_ENABLED
	orr		x1, x1, x2
#endif /* !__APCFG_SUPPORTED__ */
#endif /* HAS_APPLE_PAC */
	cmp		x0, x1
	bne		.

#if (!CONFIG_KERNEL_INTEGRITY || (CONFIG_KERNEL_INTEGRITY && !defined(KERNEL_INTEGRITY_WT)))
	/* Watchtower
	 *
	 * If we have a Watchtower monitor it will setup CPACR_EL1 for us, touching
	 * it here would trap to EL3.
	 */

	// Enable NEON
	mov		x0, #(CPACR_FPEN_ENABLE)
	msr		CPACR_EL1, x0
#endif

	// Clear thread pointer
	msr		TPIDR_EL1, xzr						// Set thread register


#if defined(APPLE_ARM64_ARCH_FAMILY)
	// Initialization common to all Apple targets
	ARM64_IS_PCORE x15
	ARM64_READ_EP_SPR x15, x12, ARM64_REG_EHID4, ARM64_REG_HID4
	orr		x12, x12, ARM64_REG_HID4_DisDcMVAOps
	orr		x12, x12, ARM64_REG_HID4_DisDcSWL2Ops
	ARM64_WRITE_EP_SPR x15, x12, ARM64_REG_EHID4, ARM64_REG_HID4
#endif  // APPLE_ARM64_ARCH_FAMILY

	// Read MIDR before start of per-SoC tunables
	mrs x12, MIDR_EL1

#if defined(APPLELIGHTNING)
	// Cebu <B0 is deprecated and unsupported (see rdar://problem/42835678)
	EXEC_COREEQ_REVLO MIDR_CEBU_LIGHTNING, CPU_VERSION_B0, x12, x13
	b .
	EXEC_END
	EXEC_COREEQ_REVLO MIDR_CEBU_THUNDER, CPU_VERSION_B0, x12, x13
	b .
	EXEC_END
#endif

	APPLY_TUNABLES x12, x13



#if HAS_CLUSTER
	// Unmask external IRQs if we're restarting from non-retention WFI
	mrs		x9, ARM64_REG_CYC_OVRD
	and		x9, x9, #(~(ARM64_REG_CYC_OVRD_irq_mask | ARM64_REG_CYC_OVRD_fiq_mask))
	msr		ARM64_REG_CYC_OVRD, x9
#endif

	// If x21 != 0, we're doing a warm reset, so we need to trampoline to the kernel pmap.
	cbnz	x21, Ltrampoline

	// Set KVA of boot args as first arg
	add		x0, x20, x22
	sub		x0, x0, x23

#if KASAN
	mov	x20, x0
	mov	x21, lr

	// x0: boot args
	// x1: KVA page table phys base
	mrs	x1, TTBR1_EL1
	bl	EXT(kasan_bootstrap)

	mov	x0, x20
	mov	lr, x21
#endif

	// Return to arm_init()
	ret

Ltrampoline:
	// Load VA of the trampoline
	adrp	x0, arm_init_tramp@page
	add		x0, x0, arm_init_tramp@pageoff
	add		x0, x0, x22
	sub		x0, x0, x23

	// Branch to the trampoline
	br		x0

/*
 * V=P to KVA trampoline.
 *	x0 - KVA of cpu data pointer
 */
	.text
	.align 2
arm_init_tramp:
	/* On a warm boot, the full kernel translation table is initialized in
	 * addition to the bootstrap tables. The layout is as follows:
	 *
	 *  +--Top of Memory--+
	 *         ...
	 *  |                 |
	 *  |  Primary Kernel |
	 *  |   Trans. Table  |
	 *  |                 |
	 *  +--Top + 5 pages--+
	 *  |                 |
	 *  |  Invalid Table  |
	 *  |                 |
	 *  +--Top + 4 pages--+
	 *  |                 |
	 *  |    KVA Table    |
	 *  |                 |
	 *  +--Top + 2 pages--+
	 *  |                 |
	 *  |    V=P Table    |
	 *  |                 |
	 *  +--Top of Kernel--+
	 *  |                 |
	 *  |  Kernel Mach-O  |
	 *  |                 |
	 *         ...
	 *  +---Kernel Base---+
	 */


	mov		x19, lr
#if defined(HAS_VMSA_LOCK)
	bl		EXT(vmsa_lock)
#endif
	// Convert CPU data PA to VA and set as first argument
	mov		x0, x21
	bl		EXT(phystokv)

	mov		lr, x19

	/* Return to arm_init() */
	ret

//#include	"globals_asm.h"

/* vim: set ts=4: */
```