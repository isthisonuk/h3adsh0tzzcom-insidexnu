---
title: Early Kernel Bringup
---

The main focus of this series so far has been iBoot. We've looked at how the SecureROM relocates and loads iBoot, we've covered iBoot tasking system, boot modes, image4 handling and Kernel bootstrap. An understanding of how the kernel is loaded is useful before we continue. 

While covering iBoot we haven't had access to the source code, making it more difficult to understand exactly what's going on. Now, as XNU is opensource, I can link to the exact code snippets that we're looking at.

In this part, we will cover the following:
* Basic AArch64 assembly code. 
* Reset vectors
* Fetching kernel boot arguments
* Exception and interrupt stack pointers
* Generic C init routine & the Link Register.
* Bootstrap page tables
* Kernel cache header mapping
* Bootstrap mapping 

-------
Finally, we start to look at how XNU works. Up until this point iBoot will have fetched the Kernel image from storage, where it will have been stored as a compressed Image4 file. The kernel is then unwrapped from the Image4, authenticated, decompressed and loaded into memory. iBoot will then jump to the base of the image in memory, and begin executing. 

At the base of the Kernel image is the `__start` trampoline. In Computer Science, a Trampoline is a memory location which holds addresses pointing to interrupt service routines, I/O routines, etc. The CPU will jump into the trampoline, and immediately jump back out. Let's look at this `__start` code.

```aarch64
    /* xnu/osfmk/arm64/start.s:317 */
    .align 3
    .globl EXT(_start)
LEXT(_start)
    b   EXT(start_first_cpu)
```
Before we start discussing what this code does, we'll start by covering these `EXT` and `LEXT` macros and what their role is, as they are used very frequently throught this code. They are defined in [`osfmk/arm64/asm.h`]() and their purpose is to prepend a character to the argument passed to them. Along with the two already shown, there is a third: `LCL`. 

* `EXT` prepends an underscore to the argument. So, calling `EXT(a)` will compile to `_a`.
* `LEXT` does the same as `EXT`, but also appends a colon. So, calling `LEXT(a)` will compile to `_a:`.
* `LCL` prepends an 'L' to the argument. So, calling `LCL(a)` will compile to `La`.

The purpose of these macros is to declare functions and labels in assembly code. 

Now, looking at the `_start` function - we are using the `.globl` directive to declare that this should be accessible globally, and the name being passed to this is `EXT(_start)`, so really what this would compile to is `__start`. Similarly with `LEXT(_start)`, this would compile as `__start:` and therefore declaring a label, much like how labels can be declared in C. To make this a bit more simple to understand, this is what the code looks like without these macros:
```
    .align 3
    .globl __start
__start:
    b   _start_first_cpu
```
Just before the `.globl` directive, however, there is an `.align 3`. This aligns the function by a specified number of bytes. Aligning by 3, we're essentially performing `_start & ~0xfff`. 

The only instruction executed here is a branch instruction to `_start_first_cpu`. A single `b` branch will jump to the function and never return. There are a few types of branch instruction though, a Branch with Link, `bl`, which will return to the caller, and branches with conditions that can be used after compare (`cmp`) instructions.

AArch64 has what is named a Link Register (`LR`). When a Branch with Link is executed the current address is written into the Link Register. The CPU will jump to the branch label, and, after a `ret` instruction, will return to the address in the Link Register. You can read more about branch instructions [here](https://developer.arm.com/documentation/dui0802/b/A32-and-T32-Instructions/B--BL--BX--and-BLX).

### Cold boot initialisation routine.

The `_start` trampoline jumps to the `_start_first_cpu` function. This code ...

The Kernel boot arguments are stored in memory at and address currently pointed at by the value in `x0`. AArch64 uses registers `x0-x7` to store parameters when calling functions, to avoid the address of the boot arguments being overwritten, the value is moved to `x20` before setting the reset vector base.

```aarch64
    .align 2
    .globl EXT(start_first_cpu)
LEXT(start_first_cpu)

    // Unlock core for debugging
    msr     OSLAR_EL1, xzr
    msr     DAIFSet, #(DAIFSC_ALL)      // Disable Interrupts

    // Move boot arguments to x20
    mov     x20, x0
    mov     x21, #0
```
#### Reset Vector

Next is to set the reset vector. This is done early as it allows the CPU to reset correctly if an issue occurs. `LowExceptionVectorBase` is a function that provides an early boot exception vector. It allows for the CPU to spin and preserve exception information should an exception occur. An Exception Vector is essentially a table that defines which handlers to use for different exceptions, as an IRQ would need to be handled differently from a Synchronous Abort.

The reset vector is set first by obtaining the address of the memory page in which the exception vector table lives. The `adrp` instruction will calculate the page address of the label, and store it in the register on the left. `x0` has now been overwritten with this page address. However, `x0` doesn't actually point to `LowExceptionVectorBase`, only to the base of the page. The next instruction, the `add`, will get the offset of the label in the page using `@pageoff`, and add this to `x0`.

The `MSR_VBAR_EL1_X0` instruction is actually a macro. As XNU supports different security features, some operations have to be verified. In this case, we're intending to set the `VBAR_EL1` register, which holds the Vector Base address for EL1. For the sake of simplicity, we're ignoring things such as KTRR and CTRR in this book, so for us `MSR_VBAR_EL1_X0` simply expands to `msr VBAR_EL1, x0`. The `msr` instruction allows a value in a General Purpose register to be moved into a System Register. 
```
    adrp    x0, EXT(LowExceptionVectorBase)@page
    add     x0, x0, EXT(LowExceptionVectorBase)@pageoff
    MSR_VBAR_EL1_X0
```

#### Fetching Kernel Boot Arguments

You'll see now why the boot arguments pointer was moved from `x0`. It's safely stored, out of the way, in `x20`. Now, `x20` is just pointing to those boot arguments in memory, what if we actually need to use them? The boot arguments look like so:

```
/* find xnu boot args struct in iboot */
```

There are four boot argument value that are being loaded here: Kernel virtual memory base, physical memory base, physical memory size, and boot flags. These are loaded using the `ldr` instruction. You'll see we have the following syntax: `ldr x22, [x20, BA_VIRT_BASE]`, what's happening here is we're taking the memory address pointed to by `x20` (the boot args), incremented it by the offset `BA_VIRT_BASE`, and loading that value into `x22`. This is done for those four boot arguments.

There's another `adrp` instruction here too. This is loading the start of the page tables into `x25`. 

```
    ldr     x22, [x20, BA_VIRT_BASE]            // kernel virt base
    ldr     x23, [x20, BA_PHYS_BASE]            // kernel phys base
    ldr     x24, [x20, BA_MEM_SIZE]             // physical memory size
    adrp    x25, EXT(bootstrap_pagetables)@page // start of page tables
    ldr     x26, [x20, BA_BOOT_FLAGS]           // kernel boot flags
```
The `TPIDRRO_EL0` register stores the userspace thread pointer and CPU number. The system may not always boot from CPU0, so this register is cleared. It will be updated later on.
```
    msr     TPIDRRO_EL0, x21
```

#### Exception & Interrupt Stack Pointers

Setting the Exception and Interrupt stack pointer is relatively the same process. The exception stack is set first. First the address of the page `excepstack_top` lives in is loaded into x0, and the offset added onto that, so `x0` now holds a pointer to `excepstack_top`. That pointer is then converted to a Kernel Virtual Address, by adding the value in `x22`, being the kernel virtual base address, to `x0`. The physical base address is then subtracted from that, this is to calculate the offset of the exception stack relative to the kernel virtual address base. We need this offset as we are working in virtual address space. 

Each exception level has it's own stack pointer. As the Kernel runs in EL1, now we've calculated the exception stack offset we want to set the stack pointer value. First we need to select the EL1 stack pointer by using `msr` to set `SPSel` - a register that control the currently selected stack pointer. The `sp` can then be set to the offset of the exception stack.

```
    // Exception stack
    adrp    x0, EXT(excepstack_top)@page        // Load top of exception stack
    add     x0, x0, EXT(excepstack_top)@pageoff
    add     x0, x0, x22                         // Convert to KVA
    sub     x0, x0, x23

    msr     SPSel, #1
    mov     sp, x0
```
The exact same process is repeated for the interrupt stack pointer, the only difference being is that instead of the EL1 stack pointer being assigned, it's `SP_EL0` instead. 
```
    // Interrupt stack
    adrp    x0, EXT(intstack_top)@page          // Load top of interrupt stack
    add     x0, x0, EXT(intstack_top)@pageoff
    add     x0, x0, x22                         // Convert to KVA
    sub     x0, x0, x23

    msr     SPSel, #0                           // Set SP_EL0 to interrupt stack
    mov     sp, x0
```
#### Load C init routine into Link Register

The address of the first C code to run in the kernel after boot, the `arm_init()` function, is loaded into the Link Register. This value throughout the rest of the `_start_first_cpu` will be loaded and unloaded as other branches take place. Eventually, however, execution will jump to this `arm_init()` function. We'll cover more on this later.

*VERIFY WHAT THIS ADD AND SUB WITH KVA AND KPA DOES*

#### Setup Bootstrap Page Tables

Memory management in XNU is something that will have an entire chapter/part dedicated to it - it's also something I'm still learning about. So this section on page tables is not going to be particularly in-depth, we'll just look at what the code is doing and discuss the theory later.

In a nutshell, memory mapping between virtual and physical memory is defined in a set of translation, or page, tables. Page tables are made up of pages, or blocks, of virtual addresses which provide the physical address for that memory and attributes for accessing it, for example permissions. There are multiple levels of translation tables, but we will cover this in more detail later on too.

What we have here is some code setting up the bootstrap page tables. We're creating two pages, one for the virtual to physical mapping and another for the virtual address of the trampolined kernel. This, however, requires four pages as we need to create an entry for the Level 1 and Level 2 tables.

However, the first thing we're doing is invalidating all current entries in the bootstrap page tables. This is a precaution incase there is any data already in the area of memory pointed to by `bootstrap_pagetables`. 

```
    mov     x0, #(ARM_TTE_EMPTY)                // Load invalid entry template
    mov     x1, x25                             // Start at V=P pagetable root.
    mov     x2, #(TTE_PGENTRIES)                // Load number of entries per page.
```

We're moving a few things around here:
 * `ARM_TTE_EMPTY`, defined as `0x00000000`, represents an invalid page entry template. This is loaded into `x0`.
 * You may remember that the address of `bootstrap_pagetables` was previously loaded into `x25`. This is loaded into `x1`.
 * `TTE_PGENTRIES`, which defines the number of entries for each page, is loaded into `x3`.

Let's have a look at `TTE_PGENTRIES` for a moment. TTE stands for Translation Table Entries, and this definition represents the maximum amount of entries allowed into a page table. However, XNU can use page tables that are either 16K bytes each, or 4K bytes each, therefore the correct value must be used for the CPU. Take a look at line 1146 in the file `osfmk/arm64/proc_reg.h`.

We have an `#ifdef` checking if `__ARM_16K_PG__` is defined. When building XNU these values are set depending on the architecture or CPU, in this case if `__ARM_16K_PG__` is set it means the CPU supports 16K page sizes. If the CPU does support 16K pages then the value of `TTE_PGENTRIES` is set to 16K shifted by 3, giving us a value of 2048 possible page entries. If we have 4K pages, the amount of possible entries is 512.

```
#define TTE_SHIFT           3                   /* shift width of a tte (sizeof(tte) == (1 << TTE_SHIFT))      */
#ifdef __ARM_16K_PG__
#   define TTE_PGENTRIES    (16384 >> TTE_SHIFT)           
#else
#   define TTE_PGENTRIES    (4096 >> TTE_SHIFT)
#endif
```

After those values have been moved we have a left-shift, shifting the value of `x2` left by 2 places. The value of `x2` currently being `TTE_PGENTRIES`, if we're using 16K pages the value after the shift should be `8192`, and for 4K pages `2048`.
```
    lsl     x2, x2, #2                          // x2 = (x2 << 2)
```

Next we have a do-while loop. This is used to iterate across each page table entry and invalidate it by writing the `ARM_TTE_EMPTY`. To write a loop in assembly we defined a label, `Linvalidate_bootstrap`, to denote the start of the loop, followed by the instructions to run, and a conditional jump so the loop continues until the condition we need is met. 

Looking at what this loop is doing, we have an `str` instruction and a `sub` instruction:
```
Linvalidate_bootstrap:	
	str		x0, [x1], #(1 << TTE_SHIFT)
	subs	x2, x2, #1
```
The `str` instruction is used to store a value from a register into a location in memory. There are three operands here:
 * The first, `x0` is the register holding the value we want to write to memory. Currently, it's set to `ARM_TTE_EMPTY` which we use to invalidate pages.
 * The second, `[x1]` means we are storing that `x0` value in the memory location pointed to by `x1`, which is the base of `bootstrap_pagetables`.
 * The third uses something called ["post-indexing"](http://www-mdp.eng.cam.ac.uk/web/library/enginfo/mdp_micro/lecture4/lecture4-2-5.html). What this does is increment the register holding the memory location, `x1`, by a particular value. Here we are incrementing the value of `x1` by `1 << TTE_SHIFT`, or `8`. This means each time the loop iterates `x1` will point to the next page in the `bootstrap_pagetables` for us to invalidate.

Next we have a `subs`. We're subtracting 1 from the value of `x2` and storing it back into `x2`. What we're doing here is counting the number of remaining entries in the pagetable. Notice that the subtraction is written `subs`, not `sub`? This is telling the CPU to update the Condition Flags after the operation is complete. 

What are Condition Flags you may ask? Well, AArch64 has four "Conditional Flags": Negative, Zero, Carry and Overflow. These are updated after arithmetic operations, such as `subs`, and are located in the Current Program Status Register, or `CPSR`, which looks like this:

![Current Program Status Register](/img/inside-xnu/program_status_register.png "Current Program Status Register")

After the `subs`, we have our conditional branch instruction, a `b.ne`, or Branch Not Equal. What this condition is doing is checking that the Z, or Zero, bit in the `CPSR` register above is NOT set. In other words, it's checking `Z != 1`. Each time the `b.ne` instruction is executed and the Z flag is still zero we will branch back to `Linvalidate_bootstrap`. Eventually `x2` will become zero, and the Z flag will be set by `subs`. Once this happens the condition `Z != 1` is no longer met and execution will continue past the loop. 

Here is the full loop, with C equivalent:
```
    lsl     x2, x2, #2                          // Shift by 2 for num entries on 4 pages.

Linvalidate_bootstrap:                          // do {
    str     x0, [x1], #(1 << TTE_SHIFT)         //  Invalidate and advance
    subs    x2, x2, #1                          //  entries--
    b.ne    Linvalidate_bootstrap               // } while (entries != 0)
```

#### Kernel Cache Header Mapping & TrustZone

< add a short intro >

< mention trustzone >

XNU uses the Mach-O file format, inherited from the Mach components of the kernel. I have previously written an introduction to this file format on my website before, which you can read [here](/_posts/2020-01-03-macho-file-format). 

The Kernel needs to work out whether there is any data mapped at the start of physical memory, such as TZ0, before creating the bootstrap mapping. To do this the kernel needs to figure out where it's own Mach-O header resides. If we take a look at a Mach-O binary loaded into Hopper, it doesn't have to be the kernel, we'll see the following at the very top:
```

            ; 
            ; MachO Header
            ; 
    __mh_execute_header:
        struct __macho_header64 {
            0xfeedfacf,                          // mach magic number identifier
            0x100000c,                           // cpu specifier
            0x0,                                 // machine specifier
            MH_EXECUTE,                          // type of file
            20,                                  // number of load commands
            1408,                                // the size of all the load commands
            MH_NOUNDEFS|MH_DYLDLINK|MH_TWOLEVEL|MH_PIE, // flags
            0x0                                  // reserved
        }
            ; 
            ; Load Command 0
            ; 
        struct __macho_segment_command_64 {
            LC_SEGMENT_64,                       // LC_SEGMENT_64
            0x48,                                // includes sizeof section_64 structs
            "__PAGEZERO", 0, 0, 0, 0, 0, 0,      // segment name
            0x0,                                 // memory address of this segment
            0x100000000,                         // memory size of this segment
            0x0,                                 // file offset of this segment
            0x0,                                 // amount to map from the file
            0x0,                                 // maximum VM protection
            0x0,                                 // initial VM protection
            0x0,                                 // number of sections in segment
            0                                    // flags
        }
        ...
```
What we have here is the Mach-O header. The header is essentially a struct that is written at the start of a Mach-O binary and describes exactly how the binary should be loaded. You can use [htool](/projects/htool) to view Mach-O file headers. What the kernel is going to do now is locate where this `__mh_execute_header` is and start loading values from the header.

We'll break this code where we are handling the header down into a few sections. With this first section we're doing two things. First, we are calculating the address of `__mh_execute_header` with the `adrp` and `add` instructions - we've seen this used before earlier on. Second is this `ldr` instruction, which is loading the value at offset `0x18` from the address in `x0` - meaning we're loading the value `0x18` bytes into the header. This is the flags property.

Now, why is this being loaded into the `w1` register? Well, AArch64 allows for both 64-bit and 32-bit registers. A 32-bit W register just addresses the first 32-bits of the corresponding 64-bit register. The `mach_header->flags` is a `uint32_t`, so this is why we only need the `w1` register. 

```
    adrp    x0, EXT(_mh_execute_header)@page            // load addres of mach header
    add     x0, x0, EXT(_mh_execute_header)@pageoff
    ldr     w1, [x0, #0x18]                             // load mach_header->flags (uint32_t)
```
Next we have a `tbz` instruction. This checks if a particular bit in a register is zero, and branches to a given label. This does not update the condition flags. We have the `w1` register that we just loaded the header flags into. The second operand is defining the bit to be tested, in this case `0x1f` or bit 31. Finally we have the label to jump to if the bit is not set. 

The header flag that sets bit 31 is `MH_DYLIB_IN_CACHE`. If this flag is set it's telling us that this Mach-O is a Dynamic Library that is part of the DYLD Shared Cache, and therefore is not the kernel's mach header. 
```
    tbz     w1, #0x1f, Lkernelcache_base_found          // is MH_DYLIB_IN_CACHE set?
```

If it turns out the base is not the kernel header then we need to calculate it. 

Directly after the Mach-O header we have segment commands. The first 32-bit's of a segment command describe whether it is an LC_SEGMENT_64 or a LC_SEGMENT. As we are on a 64-bit system we need to check that the segment command is 64-bit. To do this, we'll load those first 32-bit's into `w0` from `x0 + 0x20` (`0x20 == sizeof(mach_header_t)`), and verify that the value is an LC_SEGMENT_64, or `0x19`. If, for some reason, the first segment is not an LC_SEGMENT_64, the kernel will start an infinite loop. 
```
    ldr     w1, [x0, #0x20]                             // load the first segment_command->cmd.
    cmp     w1, #0x19                                   // if (w1 == LC_SEGMENT_64)
    b.ne    .                                          
```

Once the segment command has been verified we need to calculate the new kernel base address. First of all we must load the virtual memory address (`vmaddr`) of the first segment command. This should be `0x38` bytes after the base of the mach header. We then take the base of the mach header to compute a "slide".
```
    ldr     x1, [x0, #0x38]                             // x1 = segcmd->vmaddr
    sub     x1, x0, x1                                  // x1 = mach_base - vmaddr
```

Finally, to calculate the kernels address we'll load the `VM_KERNEL_LINK_ADDRESS`, which is defined in the XNU build config as `0xfffffff007004000`, into `x0`, and then add this slide value we've calculated. And there we have it, the kernel base address.
```
    MOV64   x0, VM_KERNEL_LINK_ADDRESS
    add     x0, x0, x1
```



#### Bootstrap Mapping


## Resources

 - [AArch64 Memory Model](https://developer.arm.com/documentation/102376/0100/Describing-memory-in-AArch64)