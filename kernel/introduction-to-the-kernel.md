---
layout: page
title: Introduction to the Kernel
permalink: /inside-xnu/kernel/introduction-to-the-kernel
---

Up until now we have focused on iBoot. We have very briefly covered the SecureROM, we've covered iBoot, hardware initialisation, iBoot tasks, boot modes, Image4 handling and Kernel loading. In this next section we're going to cover what XNU is, the role of a Kernel in an Operating System, the difference between a Monolithic, Micro and Hybrid Kernel, and the different platforms that the XNU Kernel is utilised. 

## What is XNU?

The name "XNU" is an acronym for "*X* is *N*ot *U*nix", and is a UNIX-like Operating System Kernel originally developed by Next for the NeXTSTEP OS, and later by Apple for Darwin-based Operating Systems once they bought out NeXT in 1996. The Kernel is used throughout the Apple ecosystem in macOS, iOS and watchOS (platforms such as iBridge and Homepod are derivatives of watchOS and it's XNU source tree). One of the only Apple platforms that do not use XNU is the Secure Enclave, which instead uses a heavily modified L2 Kernel.


## Types of Kernel

There are three types of Kernel we will focus on: Monolithic, Microkernel and Hybrid Kernel.

### Monolithic Kernel

A Monolithic Kernel Architecture is one in which the entire operating systems runs in a virtual "Kernel Space" and holds all privileges with respect to IO devices, memory, interrupts, etc. User space can interact with the kernel via a collection of System Calls, or syscalls, which implement services such as memory management, process management and filesystem access. Device drivers are impelemented as Kernel "modules", and these can be loaded and unloaded at runtime. 

The Monolithic architecture is relatively old. Due to their design they are typically larger than other kernel architectures as they implement so many different operations. Monolithic kernels can be slower, more difficult to update or modify, and generally less conveniant for the end-user.

Examples of Operating Systems that use a pure Monolithic architecture are Linux and MS-DOS (not to be confused with the Windows NT kernel).

Some advantages of this architecture is the idea of system calls that provide CPU, memory and file management, along with other OS functions. Monolithic kernels run entirely as a single process in a single address space.

Disadvantages of Monolithic kernels, however, are that a single service failing can cause an entire OS crash and any modification or changes require an entire OS change. They are also less secure as there is no component isolation in the kernel.

![Monolithic Kernel Architecture](/img/inside-xnu/monolithic-kernel-architecture.jpg "Monolithic Kernel Architecture")

### Microkernel

A Microkernel architecture differs from a Monolithic Kernel in that it splits almost all of the kernel's functionality into either Kernel or User space in virtual memory. The Kernel hosts the bare-minimum amount of functionality required to implement an operating system, only hosting basic IPC (Interprocess Communication), virtual memory and task scheduling in Kernel space. Everything else, such as filesystem management, device drivers, etc, are hosted in user space.

An example of a Microkernel is the L4 Kernel which the Secure Enclave OS is built upon.

This architecture is well-suited to microprocessors that implement multiple privilege levels. Both ARM and x86 architectures have these concepts; in the case of ARM we have Exception Level's EL0, EL1, EL2 and EL3, whereas with x86 we have Protection Rings. The Kernel would run in the most privileged level, so EL0 for ARM and Ring 0 for x86.

The advantages of Microkernel's are already clear, we improve system security by seperating functionality of the kernel into appropriate exception/privilege levels and reducing the amount of code that runs in kernel-space. System stability also benefits here as a part of the kernel could have some sort of error and just be restarted. Microkernel's are also simpler to develop and require less code - meaning possibly less bugs.

![Microkernel Architecture](/img/inside-xnu/microkernel-architecture.jpg "Microkernel Architecture")

## The XNU Kernel

The Darwin XNU Kernel is a Hybrid kernel built from the Monolithic FreeBSD and Micro Mach/OSFMK Kernels. Essentially NeXT/Apple cherry-picked the aspects of each kernel and combined them to form the XNU Hybrid Kernel. Apple also developed a framework for building device drivers called DriverKit (Originally called IOKit).

![XNU Hybrid Kernel Architecture](/img/inside-xnu/xnu-architecture.jpg "XNU Hybrid Kernel Architecture")

From FreeBSD, XNU inherits it's concepts of filesystems, networking, BSD Sockets, BSD Libraries, and POSIX threads. As for Mach/OSFMK7.3, XNU takes it's IPC, Virtual & Protected Memory, Scheduling, Preemptive Multitasking, and Console I/O. These all run in Kernel mode/space. In User mode we have our general applications, services and device drivers built with Driver Kit.

Currently, the XNU kernel has support for the x86-64 (Intel, AMD does not have "official" support), and ARM AArch64 architectures. While support for AArch32 was removed in iOS 11, the kernel still has the code to be compiled for that architecture.

The Mach kernel provides the base of XNU. It is heavily modified from OSFMK7.3 (Open Source Foundation Mach Kernel). Mach was chosen as it allows XNU to run different parts of the OS as seperate processes. Some BSD functions have been built-in to Mach. Darwin-based OS's also use the Mach-O Object File Format. 

BSD Provides XNU with the POSIX API, System Calls, UNIX process model, security policies (MAC), user and group IDs, file systems (HFS, HFS+, APFS), NFS and IPC. 

DriverKit, formally IOKit, is a device driver framework written in C++. Drivers can be written for either User space or Kernel space; User space drivers do not take the whole system down if they crash, whereas Kernel space drivers do. 



---
- what will we cover?
- what will we not cover?
    - security features like KTRR, CTRR, KIP, PPL, PAC, etc
    - 


## Sources

- https://en.wikipedia.org/wiki/Monolithic_kernel
- https://en.wikipedia.org/wiki/Microkernel
- https://en.wikipedia.org/wiki/Protection_ring
